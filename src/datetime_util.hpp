#pragma once

#include <ctime>
#include <string>

std::tm parse_date_and_time(const std::wstring& date, const std::wstring& time);
std::tm parse_date_and_time(const std::wstring& datetime);