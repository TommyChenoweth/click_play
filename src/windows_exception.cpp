#include "windows_exception.hpp"

std::wstring windows_exception::_retrieve_windows_message() {
    PWSTR windows_message = nullptr;
    FormatMessageW(FORMAT_MESSAGE_FROM_SYSTEM |
                  FORMAT_MESSAGE_IGNORE_INSERTS |
                  FORMAT_MESSAGE_ALLOCATE_BUFFER,
                  nullptr,
                  _hr,
                  MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
                  reinterpret_cast<LPWSTR>(&windows_message),
                  0,
                  nullptr);
    if(windows_message) {
        _windows_message = windows_message;
        LocalFree(windows_message);
    }
    else
        _windows_message = L"Failed to retrieve error message.";
    return _windows_message;
}

void windows_exception::_set_windows_message(const std::wstring & message) {
    _windows_message = message;
}

windows_exception::windows_exception(const std::string & what_arg, const HRESULT hr) 
    : windows_exception(what_arg.c_str(), hr)
{
}

windows_exception::windows_exception(const char * what_arg, const HRESULT hr) 
    : std::exception(what_arg)
    , _hr(hr)
{
    _windows_message = _retrieve_windows_message();
}

windows_exception::windows_exception(const char * what_arg, const HRESULT hr, const wchar_t * function, const wchar_t * file, int line) 
    : windows_exception(what_arg, hr)
{
    const std::wstring format_string = L"File: %s\nFunction: %s\nLine: %i";
    const size_t message_size = lstrlenW(function) + lstrlenW(file);
    _location.resize(message_size * 2, 0);
    swprintf_s(&_location[0], _location.size(), format_string.c_str(), file, function, line);
}

HRESULT windows_exception::hr() const {
    return _hr;
}

const wchar_t * windows_exception::windows_message() const {
    return _windows_message.c_str();
}

const wchar_t * windows_exception::location() const {
    return _location.c_str();
}
