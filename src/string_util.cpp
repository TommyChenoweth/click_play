#include "string_util.hpp"

#include <algorithm>
#include <Windows.h>

std::wstring multibyte_to_wide(const std::string& mbs)
{
    std::wstring wcs(mbs.size() + 1, 0);
    auto written = MultiByteToWideChar(CP_UTF8, 0, mbs.c_str(), sizeof(std::string::value_type) * mbs.length(), &wcs[0], wcs.size()-1);
    wcs.erase(std::remove(wcs.begin(), wcs.end(), 0), wcs.end());
    return wcs;
}

std::string wide_to_multibyte(const std::wstring& wcs)
{
    std::string str(wcs.size() + 1, 0);
    auto written = WideCharToMultiByte(CP_UTF8, 0, wcs.c_str(), wcs.length(), &str[0], sizeof(std::string::value_type) * (str.size() - 1), nullptr, nullptr);
    return str;
}
