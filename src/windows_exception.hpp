#pragma once

#include <string>
#include <Windows.h>

class windows_exception : public std::exception {
private:
    HRESULT      _hr;
    std::wstring _windows_message;
    std::wstring _location;

protected:
    std::wstring _retrieve_windows_message();
    void _set_windows_message(const std::wstring& message);

public:
    explicit windows_exception(const std::string& what_arg, const HRESULT hr);
    explicit windows_exception(const char* what_arg, const HRESULT hr);
    explicit windows_exception(const char* what_arg, const HRESULT hr, const wchar_t* function, const wchar_t* file, int line);

    HRESULT hr() const;
    const wchar_t* windows_message() const;
    const wchar_t* location() const;
};
#define WIDEN2(x) L ## x
#define WIDEN(x) WIDEN2(x)

#ifndef WINDOWS_EXCEPTION_IF_FAILED
#define WINDOWS_EXCEPTION_IF_FAILED(d, r) \
if(FAILED(r))                             \
    throw windows_exception(d, r, __LPREFIX(__FUNCTION__), WIDEN(__FILE__), __LINE__)
#endif