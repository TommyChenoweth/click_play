#pragma once

#include "task.hpp"
#include <comdef.h>
#include <memory>
#include <taskschd.h>
#include <vector>

class scheduler
{
private:
    ITaskService* _task_service;

    const std::wstring _datetime_format;
    const std::size_t _formatted_datetime_length;

    std::wstring _create_string_from_datetime(const std::tm& datetime);

    void _initialize_com();
    ITaskService* _create_task_service();
    ITaskFolder* _get_root_folder();

    std::wstring _compute_start_for_repeating_trigger(task t);
    uint8_t _compute_day_field_from_days(const days_t& days);

    void _set_registration_info_for_task(ITaskDefinition* task_def, task t);
    void _add_weekly_trigger_to_collection(ITriggerCollection* trigger_collection, task t);
    void _add_daily_trigger_to_collection(ITriggerCollection* trigger_collection, task t);
    void _add_time_trigger_to_collection(ITriggerCollection* trigger_collection, task t);
    void _add_executable_action_to_collection(IActionCollection* action_collection, task t);

    task _build_task(IRegisteredTask* registered_task);

public:
    scheduler();
    scheduler(const scheduler&) = delete;
    ~scheduler();

    void schedule_task(const task& t);
    std::vector<task> get_scheduled_tasks();
    void delete_task_by_name(const std::wstring& name);
};