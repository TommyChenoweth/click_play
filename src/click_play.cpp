#include "arg_validation.hpp"
#include "click.hpp"
#include "config.hpp"
#include "composite_logger.hpp"
#include "datetime_util.hpp"
#include "docopt.h"
#include "print_windows.hpp"
#include "scheduler.hpp"
#include "spdlog/spdlog.h"
#include "string_util.hpp"
#include "windows_exception.hpp"
#include <algorithm>
#include <iostream>
#include <string>
#include <unordered_map>
#include <windows.h>

const std::wstring config_path = L"click_play.json";

auto doc = R"(click_play

Usage:
    click_play click [--move-only]
    click_play windows [-t TITLE]
    click_play schedule (--once=<YYYY-MM-DD>|--daily|--weekly=<DAYS>) <HH:MM:SS> [-n NAME]
    click_play schedule list
    click_play schedule delete <NAME>

Options:
    -h --help            Show this screen.
    -t TITLE             Specify the window title. [default: Battle.net]
    --move-only          Do not click after moving the cursor.
    --once=<YYYY-MM-DD>  Execute once on the specified date.
    --daily              Execute daily.
    --weekly=<DAYS>      Execute weekly. Valid day values are SUN,MON,TUE,WED,THU,FRI,SAT
    -n NAME              Name the task. [default: click_play])";

composite_logger all_logs;

void initialize_logging();
std::wstring get_path_to_exe();
std::wstring set_cwd_from_path_to_exe(const std::wstring& path);
bool config_file_exists(const std::wstring& file_path);
std::wstring file_contents_to_string(const std::wstring& file_path);
config read_config_from_file(const std::wstring& file_path);
void write_config_to_file(const config& c, const std::wstring& file_path);

int main(int argc, char* argv[])
{
    // Enable run-time memory check for debug builds.
    #if defined(_DEBUG)
    _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
    #endif

    try
    {
        auto path_to_exe = get_path_to_exe();
        auto cwd = set_cwd_from_path_to_exe(path_to_exe);

        initialize_logging();

        all_logs.debug(L"Execution beginning.");
        atexit([]() { all_logs.debug(L"Execution ending."); });

        all_logs.debug(L"CWD: {}", cwd);

        all_logs.debug(L"Logging raw args...");
        for(std::size_t i = 0; i < argc; ++i)
            all_logs.debug(L"{}: {}", i, multibyte_to_wide(argv[i]));

        auto args = docopt::docopt(doc,
        { argv + 1, argv + argc },
            true, "v1.0", false);

        validate_args(args);

        config c;
        if(config_file_exists(config_path))
        {
            c = read_config_from_file(config_path);
        }
        else
        {
            all_logs.debug(L"Config file not found. Generating config file.");
            write_config_to_file(c, config_path);
            all_logs.debug(L"Config file generated.");
        }

        if(args["windows"].asBool())
        {
            auto window_title = multibyte_to_wide(args["-t"].asString());
            all_logs.debug(L"Listing windows titled \"{}\".", window_title);
            print_window_data(window_title);
        }

        if(args["click"].asBool())
        {
            all_logs.debug(L"Clicking...");
            auto bn_window_handle = find_window_handle(c.get_bn_window_class(), c.get_bn_window_title());
            restore_window_if_minimized(bn_window_handle);
            bring_window_to_front(bn_window_handle);
            move_cursor_to_relative_point(c.get_click_point_client(), bn_window_handle);
            if(!args["--move-only"].asBool())
            {
                Sleep(c.get_delay_ms());
                click_left_mouse_button();
                all_logs.info(L"Success! The left mouse button was clicked.");
            }
        }

        if(args["schedule"].asBool())
        {
            if(args["list"].asBool())
            {
                all_logs.debug(L"Listing scheduled tasks...");

                scheduler s;
                auto tasks = s.get_scheduled_tasks();

                if(tasks.empty())
                    all_logs.info(L"No tasks found.");

                for(const auto& t : tasks)
                {
                    all_logs.info(L"{}", t);
                }
            }
            else if(args["delete"].asBool())
            {
                auto task_name = multibyte_to_wide(args["<NAME>"].asString());
                all_logs.debug(L"Deleting scheduled task: [{}]", task_name);

                scheduler s;
                s.delete_task_by_name(task_name);
            }
            else
            {
                auto task_name = multibyte_to_wide(args["-n"].asString());
                all_logs.debug(L"Scheduling a task: [{}]", task_name);

                task t(task_name, path_to_exe, L"click");

                auto interval = args["--daily"].asBool() ? task_interval::daily :
                                args["--weekly"].isString() ? task_interval::weekly :
                                args["--once"].isString() ? task_interval::once :
                                task_interval::none;
                t.set_interval(interval);

                auto date = interval == task_interval::once ? multibyte_to_wide(args["--once"].asString().c_str()) : L"";
                auto time = multibyte_to_wide(args["<HH:MM:SS>"].asString().c_str());
                auto datetime = parse_date_and_time(date, time);
                t.set_datetime(datetime);

                if(interval == task_interval::weekly)
                {
                    std::unordered_map<std::string, task_day> string_to_day =
                    {
                        { "SUN", task_day::sunday },
                        { "MON", task_day::monday },
                        { "TUE", task_day::tuesday },
                        { "WED", task_day::wednesday },
                        { "THU", task_day::thursday },
                        { "FRI", task_day::friday },
                        { "SAT", task_day::saturday },
                    };

                    std::string days_arg = args["--weekly"].asString().c_str();
                    auto day_tokens = split_string(days_arg, ',');
                    days_t days;
                    for(const auto& t : day_tokens)
                    {
                        days.insert(string_to_day[t]);
                    }
                    t.set_days(days);
                }

                all_logs.debug(L"Created task - {}", t);

                scheduler s;
                s.schedule_task(t);
                all_logs.info(L"Success! Task was successfully registered.");
            }
        }
    }
    catch(const windows_exception& e)
    {
        all_logs.error(L"{}\n{}\n{}", e.what(), e.windows_message(), e.location());
        return -1;
    }
    catch(const argument_exception& e)
    {
        all_logs.error(L"{}", e.what());
        return -1;
    }
    catch(const docopt::DocoptLanguageError& e)
    {
        all_logs.error(L"{}", multibyte_to_wide(e.what()));
        return -1;
    }
    catch(const std::exception& e)
    {
        all_logs.error(L"{}", multibyte_to_wide(e.what()));
        return -1;
    }

	return 0;
}

void initialize_logging()
{
    const std::wstring log_file_name = L"log";
    const std::wstring log_file_extension = L"txt";
    auto full_file_name = log_file_name + L"." + log_file_extension;
    full_file_name.erase(std::remove(full_file_name.begin(), full_file_name.end(), 0), full_file_name.end());

    // Attempt to open the log file. If the file can't be opened and we pass it to
    // spdlog, spdlog will throw an exception.
    FILE* log_file = nullptr;
    _wfopen_s(&log_file, full_file_name.c_str(), L"ab");
    bool log_file_exists = !!log_file;
    if(log_file_exists)
        fclose(log_file);

    try
    {
        const size_t q_size = 1048576;
        spdlog::set_async_mode(q_size);

        {
            std::vector<spdlog::sink_ptr> sinks;
            sinks.push_back(std::make_shared<spdlog::sinks::stdout_sink_mt>());
            auto console_logger = std::make_shared<spdlog::logger>(L"logger", begin(sinks), end(sinks));
            console_logger->set_pattern(L"%v");
            spdlog::register_logger(console_logger);
            console_logger->set_level(spdlog::level::info);
            all_logs.add_logger(console_logger);
        }

        if(log_file_exists)
        {
            std::vector<spdlog::sink_ptr> file_sinks;
            file_sinks.push_back(std::make_shared<spdlog::sinks::rotating_file_sink_mt>(log_file_name.c_str(), log_file_extension.c_str(), 1024 * 500, 3));
            auto file_logger = std::make_shared<spdlog::logger>(L"file_logger", begin(file_sinks), end(file_sinks));
            file_logger->set_pattern(L"[%Y-%m-%d %H:%M:%S.%e] [%l] %v");
            spdlog::register_logger(file_logger);
            file_logger->set_level(spdlog::level::debug);
            all_logs.add_logger(file_logger);
        }
    }
    catch(const spdlog::spdlog_ex& e)
    {
        std::wcerr << e.what() << std::endl;
    }
}

bool config_file_exists(const std::wstring& file_path)
{
    FILE* config_file = nullptr;
    _wfopen_s(&config_file, file_path.c_str(), L"r");
    bool config_file_exists = !!config_file;
    if(config_file_exists)
        fclose(config_file);
    return config_file_exists;
}

std::wstring get_path_to_exe()
{
    std::wstring path_to_exe(512, 0);
    GetModuleFileName(0, &path_to_exe[0], path_to_exe.size() - 1);
    // Lose the trailing null characters
    return path_to_exe.c_str();
}

std::wstring set_cwd_from_path_to_exe(const std::wstring& path)
{
    const std::wstring dir(&path[0], &path[path.find_last_of('\\')]);
    HRESULT hr = SetCurrentDirectory(dir.c_str());
    WINDOWS_EXCEPTION_IF_FAILED("Failed to set current working directory.", hr);
    return dir;
}

std::wstring file_contents_to_string(const std::wstring& file_path)
{
    FILE* file = nullptr;
    _wfopen_s(&file, file_path.c_str(), L"r,ccs=UTF-8");
    if(!file) throw std::ios_base::failure("Failed to open file.");

    // Determine the size of the file's contents.
    fseek(file, 0, SEEK_END);
    auto file_size = ftell(file);
    fseek(file, 0, SEEK_SET);

    std::wstring file_data(file_size + 1, 0);
    fread(&file_data[0], sizeof(std::wstring::value_type), file_data.size() - 1, file);

    fclose(file);
    file = nullptr;

    // Lose the unicode BOM if it's present
    file_data.erase(std::remove(file_data.begin(), file_data.end(), 0xFEFF), file_data.end());

    return file_data;
}

config read_config_from_file(const std::wstring& file_path)
{
    auto json_string = file_contents_to_string(file_path);
    return build_config_from_json(json_string);
}

void write_config_to_file(const config& c, const std::wstring& file_path)
{
    auto json_string = build_json_from_config(c);

    FILE* file = nullptr;
    _wfopen_s(&file, file_path.c_str(), L"w,ccs=UTF-8");
    if(!file) throw std::ios_base::failure("Failed to create file.");

    fwrite(&json_string[0], sizeof(std::wstring::value_type), json_string.size(), file);
    fclose(file);
    file = nullptr;
}