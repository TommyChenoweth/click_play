#pragma once

#include <cstdint>
#include <iostream>
#include <set>
#include <string>

enum class task_interval
{
    none = 0,
    once,
    daily,
    weekly
};

enum class task_day
{
    none = 0,
    sunday,
    monday,
    tuesday,
    wednesday,
    thursday,
    friday,
    saturday
};

using days_t = std::set<task_day>;

class task
{
private:
    std::wstring _name;
    std::wstring _description;
    std::wstring _author;
    task_interval _interval;
    days_t _days;
    std::wstring _path_to_exe;
    std::wstring _args;
    std::tm _datetime;

public:
    task(const std::wstring& name, const std::wstring& path_to_exe, const std::wstring& args);
    task();

    std::wstring get_name() const;
    void set_name(const std::wstring& name);

    std::wstring get_description() const;
    void set_description(const std::wstring& description);

    std::wstring get_author() const;
    void set_author(const std::wstring& author);

    task_interval get_interval() const;
    void set_interval(const task_interval interval);

    days_t get_days() const;
    void set_days(const days_t& days);

    std::wstring get_path_to_exe() const;
    void set_path_to_exe(const std::wstring& path_to_exe);

    std::wstring get_args() const;
    void set_args(const std::wstring& args);

    std::tm get_datetime() const;
    void set_datetime(const tm& datetime);

    static std::wstring get_default_author();
};

std::wostream& operator<<(std::wostream& os, const task& t);