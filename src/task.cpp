#include "task.hpp"

#include <algorithm>
#include <unordered_map>

std::wstring task::get_default_author()
{
    return L"click_play";
}

std::unordered_map<task_interval, wchar_t*> interval_to_string =
{
    {task_interval::none,   L"none"},
    {task_interval::once,   L"once"},
    {task_interval::daily,  L"daily"},
    {task_interval::weekly, L"weekly"},
};

std::unordered_map<task_day, wchar_t*> day_to_string =
{
    {task_day::sunday,    L"Sunday"},
    {task_day::monday,    L"Monday"},
    {task_day::tuesday,   L"Tuesday"},
    {task_day::wednesday, L"Wednesday"},
    {task_day::thursday,  L"Thursday"},
    {task_day::friday,    L"Friday"},
    {task_day::saturday,  L"Saturday"},
};

task::task(const std::wstring& name, const std::wstring& path_to_exe, const std::wstring& args)
    : _name(name)
    , _description(L"This task was automatically created by click_play.")
    , _author(task::get_default_author())
    , _interval(task_interval::none)
    , _days({})
    , _path_to_exe(path_to_exe)
    , _args(args)
    , _datetime({})
{
}

task::task()
    : _name(L"")
    , _description(L"")
    , _author(task::get_default_author())
    , _interval(task_interval::none)
    , _days({})
    , _path_to_exe(L"")
    , _args(L"")
    , _datetime({})
{
}

std::wstring task::get_name() const
{
    return _name;
}

void task::set_name(const std::wstring& name)
{
    _name = name;
}

std::wstring task::get_description() const
{
    return _description;
}

void task::set_description(const std::wstring& description)
{
    _description = description;
}

std::wstring task::get_author() const
{
    return _author;
}

void task::set_author(const std::wstring& author)
{
    _author = author;
}

task_interval task::get_interval() const
{
    return _interval;
}

void task::set_interval(const task_interval interval)
{
    _interval = interval;
}

days_t task::get_days() const
{
    return _days;
}

void task::set_days(const days_t& days)
{
    _days = days;
}

std::wstring task::get_path_to_exe() const
{
    return _path_to_exe;
}

void task::set_path_to_exe(const std::wstring& path_to_exe)
{
    _path_to_exe = path_to_exe;
}

std::wstring task::get_args() const
{
    return _args;
}

void task::set_args(const std::wstring& args)
{
    _args = args;
}

std::tm task::get_datetime() const
{
    return _datetime;
}

void task::set_datetime(const std::tm& datetime)
{
    _datetime = datetime;
}

std::wostream& operator<<(std::wostream& os, const task& t)
{
    os << L"[" << t.get_name() << L"]: runs at ";

    const std::wstring at_format = t.get_interval() == task_interval::once ? L"%H:%M:%S on %Y-%m-%d." : L"%H:%M:%S";
    std::wstring datetime(30, 0);
    std::wcsftime(&datetime[0], datetime.size() - 1, at_format.c_str(), &t.get_datetime());
    datetime.erase(std::remove(datetime.begin(), datetime.end(), 0), datetime.end());
    os << datetime;

    if(t.get_interval() == task_interval::daily)
    {
        os << L" every day.";
    }
    else if(t.get_interval() == task_interval::weekly)
    {
        os << L" on ";
        auto days = t.get_days();
        uint8_t i = 0;
        for(const auto& d : t.get_days())
        { 
            if(i++ != 0)
                os << L", ";
            os << day_to_string[d];
        }
        os << L" every week.";
    }

    return os;
}