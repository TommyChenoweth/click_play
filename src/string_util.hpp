#pragma once

#include <string>
#include <vector>
#include <Windows.h>

template<typename T>
std::vector<T> split_string(const T& s, typename T::value_type delim)
{
    std::vector<T> tokens;

    std::size_t last_end = 0;
    for(std::size_t i = 0; i < s.size(); ++i)
    {
        if(s[i] == delim)
        {
            auto token = s.substr(last_end, i - last_end);
            tokens.push_back(token);

            // Skip over the delim
            last_end = i + 1;
        }
    }

    // The string probably won't end in a delim, so we'll likely need
    // to grab the trailing token
    if(last_end != s.size())
    {
        auto token = s.substr(last_end, s.size() - last_end);
        tokens.push_back(token);
    }

    return tokens;
}

std::wstring multibyte_to_wide(const std::string& mbs);
std::string wide_to_multibyte(const std::wstring& wcs);