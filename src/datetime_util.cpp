#include "datetime_util.hpp"

#include <algorithm>
#include <iomanip>
#include <sstream>

std::tm parse_date_and_time(const std::wstring& date, const std::wstring& time_string)
{
    const std::wstring valid_date = date != L"" ? date : L"1900-01-00";
    auto dts = valid_date + L"T" + time_string;
    dts.erase(std::remove(dts.begin(), dts.end(), 0), dts.end());
    return parse_date_and_time(dts);
}


std::tm parse_date_and_time(const std::wstring& datetime)
{
    const std::wstring datetime_format = L"%Y-%m-%dT%H:%M:%S";

    std::locale loc("");
    const std::time_get<wchar_t>& tmget = std::use_facet<std::time_get<wchar_t> >(loc);

    std::tm tm = {};
    std::ios::iostate state;
    std::wistringstream ss(datetime);
    tmget.get(ss, std::time_get<wchar_t>::iter_type(), ss, state, &tm,
        datetime_format.data(), datetime_format.data() + datetime_format.length());

    // Check for DST if no date was specified.
    if(!tm.tm_mday && !tm.tm_mon && !tm.tm_year)
    {
        auto current_time = time(nullptr);
        auto dst = localtime(&current_time);
        tm.tm_isdst = dst->tm_isdst;
    }

    return tm;
}
