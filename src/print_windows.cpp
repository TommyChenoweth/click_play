#pragma once

#include "spdlog/spdlog.h"
#include <array>
#include <iostream>
#include <windows.h>

BOOL CALLBACK EnumWindowsProc(HWND hwnd, LPARAM lParam)
{
    const wchar_t* title_to_find = reinterpret_cast<const wchar_t*>(lParam);

    std::array<wchar_t, 80> title;
	GetWindowText(hwnd, title.data(), title.size());

    if(wcscmp(title.data(), title_to_find) != 0)
    {
        return TRUE;
    }

    std::array<wchar_t, 80> class_name;
	GetClassName(hwnd, class_name.data(), class_name.size());

	RECT client_dimensions = {};
	GetClientRect(hwnd, &client_dimensions);

    RECT window_pos = {};
    GetWindowRect(hwnd, &window_pos);

    spdlog::get(L"logger")->info(L"Title: {}\nClass: {}\nWidth: {}\nHeight: {}\nTopleft: <{}, {}>\n",
        title.data(), class_name.data(), client_dimensions.right, client_dimensions.bottom, window_pos.left, window_pos.right);

	return TRUE;
}

void print_window_data(const std::wstring& window_title)
{
    LPARAM pointer_to_title = reinterpret_cast<LPARAM>(window_title.c_str());
	EnumWindows(EnumWindowsProc, pointer_to_title);
}
