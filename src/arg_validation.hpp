#pragma once

#include "docopt_value.h"
#include <map>
#include <string>

void validate_args(std::map<std::string, docopt::value> args);

class argument_exception : public std::exception
{
private:
    std::string create_error_message(const std::string& name, const std::string& value) const;

public:
    argument_exception(std::string name, std::string value);
};