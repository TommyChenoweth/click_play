#include "arg_validation.hpp"

#include "string_util.hpp"
#include <functional>
#include <regex>

std::map<std::string, std::function<void(std::string,docopt::value)>> arg_to_validator =
{
    {
        "--weekly",
        [](std::string name, docopt::value v)
        {
            if(!v.isString())
                return;

            auto days = v.asString();
            std::regex r("(SUN|MON|TUE|WED|THU|FRI|SAT)(,(SUN|MON|TUE|WED|THU|FRI|SAT)){0,6}");
            if(!std::regex_match(days, r))
            {
                throw argument_exception(name, days);
            }

            // Check for duplicate days
            std::map<std::string, bool> token_map;
            auto tokens = split_string(days, ',');
            for(const auto& t : tokens)
            {
                auto it = token_map.find(t);
                if(it == token_map.end())
                {
                    token_map.insert({ t, true });
                }
                else
                {
                    throw argument_exception(name, days);
                }
            }
        }
    },
    {
        "<HH:MM:SS>",
        [](std::string name, docopt::value v)
        {
            if(!v.isString())
                return;

            auto time = v.asString();
            std::regex r("([0-1][0-9]|2[0-3])(:([0-5][0-9])){2}");
            if(!std::regex_match(time, r))
            {
                throw argument_exception(name, v.asString());
            }
        }
    },
    {
        "--once",
        [](std::string name, docopt::value v)
        {
            if(!v.isString())
                return;

            auto date = v.asString();
            std::regex r("[0-9]{4}-(0[1-9]|1[0-2])-([0-2][0-9]|3[0-1])");
            if(!std::regex_match(date, r))
            {
                throw argument_exception(name, v.asString());
            }
        }
    },
};

void validate_args(std::map<std::string, docopt::value> args)
{
    for(const auto& arg : args)
    {
        auto name = arg.first;
        auto it = arg_to_validator.find(name);
        if(it != arg_to_validator.end())
        {
            auto value = arg.second;
            auto validate = it->second;
            validate(name, value);
        }
    }
}


std::string argument_exception::create_error_message(const std::string& name, const std::string& value) const
{
    return "Invalid value \"" + value + "\" for argument \"" + name + "\"";
}

argument_exception::argument_exception(std::string name, std::string value)
    : std::exception(create_error_message(name, value).c_str())
{
}