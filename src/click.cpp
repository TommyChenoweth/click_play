#include "click.hpp"

#include <iostream>

HWND find_window_handle(const std::wstring& window_class, const std::wstring& window_title)
{
	HWND window_handle = FindWindow(window_class.c_str(), window_title.c_str());
	if(!window_handle)
	{
        throw std::exception("Failed to find window handle.");
	}
    return window_handle;
}

void restore_window_if_minimized(const HWND window_handle)
{
    WINDOWPLACEMENT wp = {};
	wp.length = sizeof(wp);
    if(!GetWindowPlacement(window_handle, &wp))
    {
        throw std::exception("Failed to retrieve window state.");
    }
	if((wp.showCmd & SW_SHOWMINIMIZED) != 0)
	{
		wp.showCmd ^= SW_SHOWMINIMIZED;
		wp.showCmd |= SW_SHOWNORMAL;
        if(!SetWindowPlacement(window_handle, &wp))
        {
            throw std::exception("Failed to restore the window.");
        }
	}
}

void bring_window_to_front(const HWND window_handle)
{
    if(!SetForegroundWindow(window_handle))
    {
        throw std::exception("Failed to bring window to front.");
    }
}

void move_cursor_to_relative_point(const POINT& relative_point, const HWND window_handle)
{
    RECT window_dimensions = {};
	if(!GetClientRect(window_handle, &window_dimensions))
	{
        throw std::exception("Failed to get window client dimensions.");
	}

    // Transform to absolute coordinates
    POINT absolute_point = { window_dimensions.left + relative_point.x, window_dimensions.bottom + relative_point.y };
    if(!ClientToScreen(window_handle, &absolute_point))
    {
        throw std::exception("Failed to transform coordinates to screen space.");
    }

    // Move the cursor
    if(!SetCursorPos(absolute_point.x, absolute_point.y))
    {
        throw std::exception("Failed to move the cursor.");
    }
}

void click_left_mouse_button()
{
	INPUT mouse_input = {};
	mouse_input.mi.dwFlags = MOUSEEVENTF_LEFTDOWN | MOUSEEVENTF_LEFTUP;
	if(SendInput(1, &mouse_input, sizeof(mouse_input)) != 1)
	{
        throw std::exception("Failed to send input.");
	}
}

