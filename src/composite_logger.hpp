#include "spdlog/spdlog.h"
#include <vector>

class composite_logger
{
private:
    std::vector<std::shared_ptr<spdlog::logger>> _loggers;

public:
    void add_logger(std::shared_ptr<spdlog::logger> l)
    {
        _loggers.push_back(l);
    }

    template <typename Arg1, typename... Args> void trace(const wchar_t* fmt, const Arg1& arg1, const Args&... args)
    {
        for(auto& l : _loggers)
            l->trace(fmt, arg1, args...);
    }

    template <typename Arg1, typename... Args> void debug(const wchar_t* fmt, const Arg1& arg1, const Args&... args)
    {
        for(auto& l : _loggers)
            l->debug(fmt, arg1, args...);
    }

    template <typename Arg1, typename... Args> void info(const wchar_t* fmt, const Arg1& arg1, const Args&... args)
    {
        for(auto& l : _loggers)
            l->info(fmt, arg1, args...);
    }

    template <typename Arg1, typename... Args> void notice(const wchar_t* fmt, const Arg1& arg1, const Args&... args)
    {
        for(auto& l : _loggers)
            l->notice(fmt, arg1, args...);
    }

    template <typename Arg1, typename... Args> void warn(const wchar_t* fmt, const Arg1& arg1, const Args&... args)
    {
        for(auto& l : _loggers)
            l->warn(fmt, arg1, args...);
    }

    template <typename Arg1, typename... Args> void error(const wchar_t* fmt, const Arg1& arg1, const Args&... args)
    {
        for(auto& l : _loggers)
            l->error(fmt, arg1, args...);
    }

    template <typename Arg1, typename... Args> void critical(const wchar_t* fmt, const Arg1& arg1, const Args&... args)
    {
        for(auto& l : _loggers)
            l->critical(fmt, arg1, args...);
    }

    template <typename Arg1, typename... Args> void alert(const wchar_t* fmt, const Arg1& arg1, const Args&... args)
    {
        for(auto& l : _loggers)
            l->alert(fmt, arg1, args...);
    }

    template <typename Arg1, typename... Args> void emerg(const wchar_t* fmt, const Arg1& arg1, const Args&... args)
    {
        for(auto& l : _loggers)
            l->emerge(fmt, arg1, args...);
    }

    template <typename T> void trace(const T& msg)
    {
        for(auto& l : _loggers)
            l->trace(msg);
    }

    template <typename T> void debug(const T& msg)
    {
        for(auto& l : _loggers)
            l->debug(msg);
    }

    template <typename T> void info(const T& msg)
    {
        for(auto& l : _loggers)
            l->info(msg);
    }

    template <typename T> void notice(const T& msg)
    {
        for(auto& l : _loggers)
            l->notice(msg);
    }

    template <typename T> void warn(const T& msg)
    {
        for(auto& l : _loggers)
            l->warn(msg);
    }

    template <typename T> void error(const T& msg)
    {
        for(auto& l : _loggers)
            l->error(msg);
    }

    template <typename T> void critical(const T& msg)
    {
        for(auto& l : _loggers)
            l->critical(msg);
    }

    template <typename T> void alert(const T& msg)
    {
        for(auto& l : _loggers)
            l->alert(msg);
    }

    template <typename T> void emerg(const T& msg)
    {
        for(auto& l : _loggers)
            l->emerg(msg);
    }

};

extern composite_logger all_logs;