#include "config.hpp"

#include "composite_logger.hpp"
#include "json.hpp"

using json = nlohmann::json;

config::config()
    : _bn_window_title(L"Battle.net")
    , _bn_window_class(L"Qt5QWindowOwnDCIcon")
    , _click_point_client{275, -70}
    , _delay_ms(50)
{
}

void config::set_bn_window_title(const std::wstring& title)
{
    _bn_window_title = title;
}

std::wstring config::get_bn_window_title() const
{
    return _bn_window_title;
}

void config::set_bn_window_class(const std::wstring& window_class)
{
    _bn_window_class = window_class;
}

std::wstring config::get_bn_window_class() const
{
    return _bn_window_class;
}

void config::set_click_point_client(const POINT click_point)
{
    _click_point_client = click_point;
}

POINT config::get_click_point_client() const
{
    return _click_point_client;
}

void config::set_delay_ms(const uint32_t delay_ms)
{
    _delay_ms = delay_ms;
}

uint32_t config::get_delay_ms() const
{
    return _delay_ms;
}

config build_config_from_json(const std::wstring& json_string)
{
    config c;
    try
    {
        auto json_config = json::parse(json_string);

        c.set_bn_window_title(json_config[L"window"][L"title"]);
        c.set_bn_window_class(json_config[L"window"][L"class"]);
        POINT p = { json_config[L"click"][L"left"], json_config[L"click"][L"bottom"] };
        c.set_click_point_client(p);
        c.set_delay_ms(json_config[L"delay_ms"]);
    }
    catch(const std::domain_error& e)
    {
        all_logs.debug(L"json error: {}", e.what());
        throw std::exception("Unable to parse config file. Please check the formatting.");
    }

    return c;
}

std::wstring build_json_from_config(const config& c)
{
    json json_config;
    json_config[L"window"][L"title"] = c.get_bn_window_title();
    json_config[L"window"][L"class"] = c.get_bn_window_class();
    json_config[L"click"][L"left"] = c.get_click_point_client().x;
    json_config[L"click"][L"bottom"] = c.get_click_point_client().y;
    json_config[L"delay_ms"] = c.get_delay_ms();

    const uint32_t indent_spaces = 4;
    return json_config.dump(indent_spaces);
}