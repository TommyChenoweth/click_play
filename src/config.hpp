#pragma once

#include <string>
#include <windows.h>

class config
{
private:
    std::wstring _bn_window_title;
    std::wstring _bn_window_class;
    POINT _click_point_client;
    uint32_t _delay_ms;
    uint32_t _task_lifespan_days;
    
public:
    config();

    void set_bn_window_title(const std::wstring& title);
    std::wstring get_bn_window_title() const;

    void set_bn_window_class(const std::wstring& window_class);
    std::wstring get_bn_window_class() const;

    void set_click_point_client(const POINT click_point);
    POINT get_click_point_client() const;

    void set_delay_ms(const uint32_t delay_ms);
    uint32_t get_delay_ms() const;
};

config build_config_from_json(const std::wstring& json_string);
std::wstring build_json_from_config(const config& c);