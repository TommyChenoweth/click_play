#pragma once

#include <memory>

template <typename T>
std::shared_ptr<T> make_com_pointer(T* com_object)
{
	return std::shared_ptr<T> (com_object, [](T* p){p->Release();});
}

template <typename T>
std::shared_ptr<T> make_sys_str_pointer(T* sys_str)
{
    return std::shared_ptr<T>(sys_str, [](T* p){SysFreeString(p);});
}