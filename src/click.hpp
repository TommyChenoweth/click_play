#pragma once

#include <string>
#include <windows.h>

HWND find_window_handle(const std::wstring& window_class, const std::wstring& window_title);
void restore_window_if_minimized(const HWND window_handle);
void bring_window_to_front(const HWND window_handle);
void move_cursor_to_relative_point(const POINT& relative_point, const HWND window_handle);
void click_left_mouse_button();