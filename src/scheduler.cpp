#include "scheduler.hpp"

#define _WIN32_DCOM

#include "com_pointer.hpp"
#include "composite_logger.hpp"
#include "datetime_util.hpp"
#include "string_util.hpp"
#include "windows_exception.hpp"
#include <algorithm>
#include <comdef.h>
#include <iostream>
#include <stdio.h>
#include <taskschd.h>
#include <unordered_map>
#include <windows.h>

// Release COM object it goes out of scope
#ifndef COM_GUARD
#define COM_GUARD(p) \
    auto p##_guard = make_com_pointer(p)
#endif

// Free system string when it goes out of scope.
#ifndef SS_GUARD
#define SS_GUARD(p) \
    auto p##_guard = make_sys_str_pointer(p)
#endif

std::unordered_map<task_day, uint8_t> day_to_bit =
{
    { task_day::sunday,    1 << 0 },
    { task_day::monday,    1 << 1 },
    { task_day::tuesday,   1 << 2 },
    { task_day::wednesday, 1 << 3 },
    { task_day::thursday,  1 << 4 },
    { task_day::friday,    1 << 5 },
    { task_day::saturday,  1 << 6 },
};

auto reverse = [](std::unordered_map<task_day, uint8_t> tr)
{
    std::unordered_map<uint8_t, task_day> rev;
    for(const auto& el : tr)
        rev.insert({ el.second, el.first });
    return rev;
};

auto bit_to_day = reverse(day_to_bit);

std::wstring scheduler::_create_string_from_datetime(const std::tm& datetime)
{
    std::wstring time_string(_formatted_datetime_length, ' ');
    auto r = std::wcsftime(&time_string[0], time_string.length() - 1, _datetime_format.c_str(), &datetime);
    return time_string;
}
void scheduler::_initialize_com()
{
    HRESULT hr = CoInitializeEx(nullptr, COINIT_MULTITHREADED);
    WINDOWS_EXCEPTION_IF_FAILED("Failed to initialize COM.", hr);

    // Set general COM security levels.
    hr = CoInitializeSecurity(
        nullptr,
        -1,
        nullptr,
        nullptr,
        RPC_C_AUTHN_LEVEL_PKT,
        RPC_C_IMP_LEVEL_IMPERSONATE,
        nullptr,
        0,
        nullptr);
    WINDOWS_EXCEPTION_IF_FAILED("Failed to set COM security levels.", hr);
}

ITaskService* scheduler::_create_task_service()
{
    ITaskService *service = nullptr;
    HRESULT hr = CoCreateInstance( CLSID_TaskScheduler,
                           nullptr,
                           CLSCTX_INPROC_SERVER,
                           IID_ITaskService,
                           reinterpret_cast<void**>(&service));  
    WINDOWS_EXCEPTION_IF_FAILED("Failed to create a task service instance.", hr);
        
    // Connect to the task service.
    hr = service->Connect(_variant_t(), _variant_t(), _variant_t(), _variant_t());
    WINDOWS_EXCEPTION_IF_FAILED("Failed to connect to the task service.", hr);

    return service;
}

ITaskFolder* scheduler::_get_root_folder()
{
    ITaskFolder *root_folder = nullptr;
    HRESULT hr = _task_service->GetFolder(_bstr_t( L"\\") , &root_folder);
    WINDOWS_EXCEPTION_IF_FAILED("Failed to get root folder pointer.", hr);
    return root_folder;
}

std::wstring scheduler::_compute_start_for_repeating_trigger(task t)
{
    time_t current_seconds = time(nullptr);
    auto start_datetime = localtime(&current_seconds);
    auto task_datetime = t.get_datetime();
    start_datetime->tm_hour = task_datetime.tm_hour;
    start_datetime->tm_min = task_datetime.tm_min;
    start_datetime->tm_sec = task_datetime.tm_sec;
    return _create_string_from_datetime(*start_datetime);
}

uint8_t scheduler::_compute_day_field_from_days(const days_t& days)
{
    uint8_t day_field = 0;
    for(const auto& day : days)
    {
        day_field |= day_to_bit[day];
    }
    return day_field;
}
void scheduler::_set_registration_info_for_task(ITaskDefinition* task_def, task t)
{
    IRegistrationInfo* reg_info = nullptr;
    HRESULT hr = task_def->get_RegistrationInfo(&reg_info);
    WINDOWS_EXCEPTION_IF_FAILED("Failed to retrieve registration info.", hr);
    COM_GUARD(reg_info);
    
    hr = reg_info->put_Author(_bstr_t(task::get_default_author().c_str()));
    WINDOWS_EXCEPTION_IF_FAILED("Failed to set author.", hr);

    hr = reg_info->put_Description(_bstr_t(t.get_description().c_str()));
    WINDOWS_EXCEPTION_IF_FAILED("Failed to set description.", hr);
}

void scheduler::_add_weekly_trigger_to_collection(ITriggerCollection* trigger_collection, task t)
{
    ITrigger* trigger = nullptr;
    HRESULT hr = trigger_collection->Create(TASK_TRIGGER_WEEKLY, &trigger);
    WINDOWS_EXCEPTION_IF_FAILED("Failed to create trigger.", hr);
    COM_GUARD(trigger);

    IWeeklyTrigger* weekly_trigger = nullptr;
    hr = trigger->QueryInterface(IID_IWeeklyTrigger, reinterpret_cast<void**>(&weekly_trigger));
    WINDOWS_EXCEPTION_IF_FAILED("Failed to retrieve IWeeklyTrigger interface.", hr);
    COM_GUARD(weekly_trigger);
    
    hr = weekly_trigger->put_Id(_bstr_t(L"Trigger1"));
    WINDOWS_EXCEPTION_IF_FAILED("Failed to set trigger id.", hr);

    auto start_time = _compute_start_for_repeating_trigger(t);
    hr = weekly_trigger->put_StartBoundary(_bstr_t(start_time.c_str()));
    WINDOWS_EXCEPTION_IF_FAILED("Failed to set start boundary.", hr);

    auto day_field = _compute_day_field_from_days(t.get_days());
    hr = weekly_trigger->put_DaysOfWeek(day_field);
    WINDOWS_EXCEPTION_IF_FAILED("Failed to set days.", hr);
}

void scheduler::_add_daily_trigger_to_collection(ITriggerCollection* trigger_collection, task t)
{
    ITrigger* trigger = nullptr;
    HRESULT hr = trigger_collection->Create(TASK_TRIGGER_DAILY, &trigger);
    WINDOWS_EXCEPTION_IF_FAILED("Failed to create trigger.", hr);
    COM_GUARD(trigger);

    IDailyTrigger* daily_trigger = nullptr;
    hr = trigger->QueryInterface(IID_IDailyTrigger, reinterpret_cast<void**>(&daily_trigger));
    WINDOWS_EXCEPTION_IF_FAILED("Failed to retrieve IDailyTrigger interface.", hr);
    COM_GUARD(daily_trigger);

    hr = daily_trigger->put_Id(_bstr_t(L"Trigger1"));
    WINDOWS_EXCEPTION_IF_FAILED("Failed to set trigger id.", hr);

    auto start_time = _compute_start_for_repeating_trigger(t);
    hr = daily_trigger->put_StartBoundary(_bstr_t(start_time.c_str()));
    WINDOWS_EXCEPTION_IF_FAILED("Failed to set start boundary.", hr);
}

void scheduler::_add_time_trigger_to_collection(ITriggerCollection* trigger_collection, task t)
{
    ITrigger* trigger = nullptr;
    HRESULT hr = trigger_collection->Create(TASK_TRIGGER_TIME, &trigger);
    WINDOWS_EXCEPTION_IF_FAILED("Failed to create trigger.", hr);
    COM_GUARD(trigger);

    ITimeTrigger* time_trigger = nullptr;
    hr = trigger->QueryInterface(IID_ITimeTrigger, reinterpret_cast<void**>(&time_trigger));
    WINDOWS_EXCEPTION_IF_FAILED("Failed to retrieve ITimeTrigger interface.", hr);
    COM_GUARD(time_trigger);

    hr = time_trigger->put_Id(_bstr_t(L"Trigger1"));
    WINDOWS_EXCEPTION_IF_FAILED("Failed to set trigger id.", hr);

    auto start = t.get_datetime();
    auto start_time = _create_string_from_datetime(start);
    hr = time_trigger->put_StartBoundary(_bstr_t(start_time.c_str()));
    WINDOWS_EXCEPTION_IF_FAILED("Failed to set start boundary.", hr);

    // Allow the task to expire one minute from when it's supposed to trigger.
    auto end_seconds = mktime(&start) + 60;
    auto end = localtime(&end_seconds);
    auto end_time = _create_string_from_datetime(*end);
    hr = time_trigger->put_EndBoundary(_bstr_t(end_time.c_str()));
    WINDOWS_EXCEPTION_IF_FAILED("Failed set end boundary.", hr);
}

void scheduler::_add_executable_action_to_collection(IActionCollection* action_collection, task t)
{
    IAction* action = nullptr;
    HRESULT hr = action_collection->Create(TASK_ACTION_EXEC, &action);
    WINDOWS_EXCEPTION_IF_FAILED("Failed to create action.", hr);
    COM_GUARD(action);

    IExecAction* exec_action = nullptr;
    hr = action->QueryInterface(IID_IExecAction, reinterpret_cast<void**>(&exec_action));
    WINDOWS_EXCEPTION_IF_FAILED("Failed to retrieve IExecAction interface.", hr);
    COM_GUARD(exec_action);

    hr = exec_action->put_Path(_bstr_t(t.get_path_to_exe().c_str()));
    WINDOWS_EXCEPTION_IF_FAILED("Failed to set path to executable.", hr);

    hr = exec_action->put_Arguments(_bstr_t(t.get_args().c_str()));
    WINDOWS_EXCEPTION_IF_FAILED("Failed to set arguments.", hr);
}

task scheduler::_build_task(IRegisteredTask* registered_task)
{
    task out_task;

    ITaskDefinition* definition = nullptr;
    HRESULT hr = registered_task->get_Definition(&definition);
    if(SUCCEEDED(hr))
    {
        COM_GUARD(definition);
        IRegistrationInfo* reg_info = nullptr;
        definition->get_RegistrationInfo(&reg_info);
        if(SUCCEEDED(hr))
        {
            COM_GUARD(reg_info);
            BSTR bs_author = nullptr;
            hr = reg_info->get_Author(&bs_author);
            if(SUCCEEDED(hr) && bs_author)
            {
                SS_GUARD(bs_author);
                out_task.set_author(bs_author);
            }
        }

        ITriggerCollection* triggers = nullptr;
        hr = definition->get_Triggers(&triggers);
        if(SUCCEEDED(hr))
        {
            COM_GUARD(triggers);
            LONG trigger_count = 0;
            hr = triggers->get_Count(&trigger_count);
            if(SUCCEEDED(hr) && trigger_count)
            {
                ITrigger* trigger = nullptr;
                hr = triggers->get_Item(1, &trigger);
                if(SUCCEEDED(hr))
                {
                    COM_GUARD(trigger);
                    TASK_TRIGGER_TYPE2 ttt = {};
                    hr = trigger->get_Type(&ttt);
                    if(SUCCEEDED(hr))
                    {
                        auto interval =
                            ttt == TASK_TRIGGER_WEEKLY ? task_interval::weekly :
                            ttt == TASK_TRIGGER_DAILY ? task_interval::daily :
                            ttt == TASK_TRIGGER_TIME ? task_interval::once :
                            task_interval::none;
                        out_task.set_interval(interval);

                        if(interval == task_interval::weekly)
                        {
                            IWeeklyTrigger* weekly_trigger = nullptr;
                            hr = trigger->QueryInterface(IID_IWeeklyTrigger, reinterpret_cast<void**>(&weekly_trigger));
                            if(SUCCEEDED(hr))
                            {
                                COM_GUARD(weekly_trigger);
                                short days_field = 0;
                                hr = weekly_trigger->get_DaysOfWeek(&days_field);
                                if(SUCCEEDED(hr))
                                {
                                    days_t days;
                                    for(uint8_t i = 1; 0 < i; i <<= 1)
                                    {
                                        if(days_field & i)
                                        {
                                            days.insert(bit_to_day[i]);
                                        }
                                    }
                                    out_task.set_days(days);
                                }
                            }
                        }
                    }

                    BSTR bs_start_boundary = nullptr;
                    hr = trigger->get_StartBoundary(&bs_start_boundary);
                    if(SUCCEEDED(hr) && bs_start_boundary)
                    {
                        SS_GUARD(bs_start_boundary);
                        auto tm = parse_date_and_time(bs_start_boundary);
                        out_task.set_datetime(tm);
                    }
                }
            }
        }
    }

    std::wstring task_name;
    BSTR bs_task_name = nullptr;
    hr = registered_task->get_Name(&bs_task_name);
    if(SUCCEEDED(hr) && bs_task_name)
    {
        SS_GUARD(bs_task_name);
        out_task.set_name(bs_task_name);
    }

    return out_task;
}

scheduler::scheduler()
    : _task_service(nullptr)
    , _datetime_format(L"%Y-%m-%dT%H:%M:%S")
    , _formatted_datetime_length(wcslen(L"YYYY-MM-DDTHH:MM:SS") + 2)
{
    _initialize_com();
    _task_service = _create_task_service();
}

scheduler::~scheduler()
{
    CoUninitialize();
}

void scheduler::schedule_task(const task& t)
{
    auto root_folder = make_com_pointer(_get_root_folder());

    // Don't an existing task unless it was created by click_play.
    IRegisteredTask* existing_registered_task = nullptr;
    HRESULT hr = root_folder->GetTask(_bstr_t(t.get_name().c_str()), &existing_registered_task);
    if(existing_registered_task && SUCCEEDED(hr))
    {
        COM_GUARD(existing_registered_task);
        auto existing_task = _build_task(existing_registered_task);
        if(existing_task.get_author() != task::get_default_author())
        {
            auto wcs_message = L"Cannot overwrite existing task [" + t.get_name() + L"] because it wasn't created by click_play.";
            wcs_message.erase(std::remove(wcs_message.begin(), wcs_message.end(), 0), wcs_message.end());
            throw std::exception(wide_to_multibyte(wcs_message).c_str());
        }
    }
    
    //  If the same task exists, remove it.
    root_folder->DeleteTask(_bstr_t(t.get_name().c_str()), 0);
    
    //  Create the task builder object to create the task.
    ITaskDefinition* task_def = nullptr;
    hr = _task_service->NewTask(0, &task_def);
    WINDOWS_EXCEPTION_IF_FAILED("Failed to create a task definition.", hr);
    COM_GUARD(task_def);

    ITaskSettings* settings = nullptr;
    hr = task_def->get_Settings(&settings);
    WINDOWS_EXCEPTION_IF_FAILED("Failed to retrieve task settings.", hr);
    COM_GUARD(settings);
            
    _set_registration_info_for_task(task_def, t);
    
    ITriggerCollection* trigger_collection = nullptr;
    hr = task_def->get_Triggers(&trigger_collection);
    WINDOWS_EXCEPTION_IF_FAILED("Failed to get trigger collection.", hr);
    COM_GUARD(trigger_collection);

    switch(t.get_interval())
    {
    case task_interval::weekly:
        _add_weekly_trigger_to_collection(trigger_collection, t);
        break;
    case task_interval::daily:
        _add_daily_trigger_to_collection(trigger_collection, t);
        break;
    case task_interval::once:
        _add_time_trigger_to_collection(trigger_collection, t);

        // Delete the task after it's run.
        hr = settings->put_DeleteExpiredTaskAfter(L"PT0M");
        WINDOWS_EXCEPTION_IF_FAILED("Failed to set task setting.", hr);
        break;
    default:
        throw std::exception("Error: No task interval found.");
    }

    IActionCollection* action_collection = nullptr;
    hr = task_def->get_Actions(&action_collection);
    WINDOWS_EXCEPTION_IF_FAILED("Failed to retrieve action collection.", hr);
    COM_GUARD(action_collection);

    _add_executable_action_to_collection(action_collection, t);

    IRegisteredTask* registered_task = nullptr;
    hr = root_folder->RegisterTaskDefinition(
            _bstr_t(t.get_name().c_str()),
            task_def,
            TASK_CREATE_OR_UPDATE, 
            _variant_t(), 
            _variant_t(), 
            TASK_LOGON_INTERACTIVE_TOKEN,
            _variant_t(L""),
            &registered_task);
    WINDOWS_EXCEPTION_IF_FAILED("Failed to save the task.", hr);
}

std::vector<task> scheduler::get_scheduled_tasks()
{
    auto root_folder = make_com_pointer(_get_root_folder());

    IRegisteredTaskCollection* registered_tasks = nullptr;
    HRESULT hr = root_folder->GetTasks(0, &registered_tasks);
    WINDOWS_EXCEPTION_IF_FAILED("Failed to retrieve registered tasks.", hr);
    COM_GUARD(registered_tasks);

    LONG task_count = 0;
    hr = registered_tasks->get_Count(&task_count);
    WINDOWS_EXCEPTION_IF_FAILED("Failed to retrieve the number of registered tasks.", hr);

    if(!task_count)
    {
        return {};
    }

    std::vector<task> tasks;
    for(LONG i = 0; i < task_count; ++i)
    {
        IRegisteredTask* registered_task = nullptr;
        hr = registered_tasks->get_Item(_variant_t(i + 1), &registered_task);
        if(FAILED(hr)) continue;
        COM_GUARD(registered_task);

        auto t = _build_task(registered_task);
        if(t.get_author() == task::get_default_author())
        {
            tasks.push_back(t);
        }
    }
    return tasks;
}

void scheduler::delete_task_by_name(const std::wstring& name)
{
    auto root_folder = make_com_pointer(_get_root_folder());

    IRegisteredTask* registered_task = nullptr;
    HRESULT hr = root_folder->GetTask(_bstr_t(name.c_str()), &registered_task);
    if(FAILED(hr))
    {
        all_logs.info(L"Cannot locate task [{}].", name);
        return;
    }
    COM_GUARD(registered_task);

    bool is_cp_task = false;

    ITaskDefinition* definition = nullptr;
    hr = registered_task->get_Definition(&definition);
    if(SUCCEEDED(hr))
    {
        COM_GUARD(definition);
        IRegistrationInfo* reg_info = nullptr;
        definition->get_RegistrationInfo(&reg_info);
        if(SUCCEEDED(hr))
        {
            COM_GUARD(reg_info);
            BSTR bs_author = nullptr;
            hr = reg_info->get_Author(&bs_author);
            if(SUCCEEDED(hr) && bs_author)
            {
                SS_GUARD(bs_author);
                is_cp_task = wcscmp(bs_author, task::get_default_author().c_str()) == 0;
            }
        }
    }

    // Only delete the task if we can verify that we created it.
    if(!is_cp_task)
    {
        all_logs.info(L"Cannot delete task [{}] because it wasn't created by click_play.", name);
        return;
    }
    
    hr = root_folder->DeleteTask(_bstr_t(name.c_str()), 0);
    WINDOWS_EXCEPTION_IF_FAILED("Failed to delete the task.", hr);

    all_logs.info(L"Deleted task [{}].", name);
}