# click_play

click_play is a tool for scheduling the launch of the WoW client. At configurable times, click_play
will restore the Battle.net launcher (if it's minimized), bring it to the front, move your mouse
cursor over the blue "PLAY" button, and then left click.

## Features

- Automatically click the "PLAY" button on the Battle.net client.
- Use the CLI to schedule click_play to run so that you can hop into the queue even when you're not
  near your computer.

## Usage

Remember that you can see all available commands by running `click_play -h`.

### Testing

Before you do anything else, make sure that click_play's default config will work for you by
running `click_play click` when the Battle.net client isn't closed.

If click_play didn't bring the Battle.net client to the front and then click the "PLAY" button,
you may need to adjust the config. See the Configuration section below.

### Scheduling

I'll walk through some contrived scenarios to give you an understanding of how click_play can
be scheduled.

I have raids at 7:30 PM CST every Tuesday and Thursday. I want to be in queue 30 minutes before
raid time, so I run `click_play schedule --weekly=TUE,THU 19:00:00 -n raid_time`.

I'm home from work around 3:15 every day. I want to be in queue 30 minutes before then, so
I run `click_play schedule --daily 14:45:00 -n home_from_work`.

With that done, I decide that it's time to take a nap. I set my alarm for 11:00 AM. I want
to hop into the queue 20 minutes before I wake up, so I run `click_play schedule --once=2016-08-30 10:40:00`.

Next I run `click_play schedule list` to check my work, and I see this output:
~~~
[click_play]: runs at 10:40:00 on 2016-08-30.
[home_from_work]: runs at 14:45:00 every day.
[raid_time]: runs at 19:00:00 on Tuesday, Thursday every week.
~~~

Notice that when I don't assign a name to a scheduled task, it's given the name click_play.

Finally, I realize that if I'm home at 3:15, I don't need click_play to log me in before raids.
To remove the raid_time task, I run `click_play schedule delete raid_time`.

I can confirm that the task was deleted by running `click_play schedule list`, and I see:
~~~
[click_play]: runs at 10:40:00 on 2016-08-30.
[home_from_work]: runs at 14:45:00 every day.
~~~

## Configuration

click_play stores config data in click_play.json which is in the same directory as the executable.
If click_play.json is missing, it will be recreated next time click_play is run.

If the "PLAY" button on the Battle.net client is clicked when you run `click_play click`, then
you shouldn't need to adjust the configuration.

### click.bottom and click.left

To change the point to which your cusor is moved on the Battle.net client, you can adjust the
click.bottom and click.left properties; those are offsets relative to the left and bottom of
the client respectively. The "PLAY" button seems to be anchored to the bottom left corner of the
client, so you should not have to adjust this if you resize the client.

### window.class and window.title

The window.class and window.title properties are used to identify the Battle.net client's window.
There are actually multiple windows titled "Battle.net" even if you can't see them - you can
verify this by running `click_play windows` when the Battle.net window _isn't_ closed - so
we need to consider the window class when we select the client window. You don't need to worry
about this values unless you see the error message `Failed to find window handle.`. If you see
that message, run `click_play windows` to identify the correct title and class, and then update
the values in the config file.

### delay_ms

The delay_ms property is used to add a delay (in milliseconds) between when the Battle.net
client is brought to the front and when the cursor is clicked. To put it another way, the
default value of 50 means that after click_play restores the client and brings it to the front,
it waits 50 ms before clicking. Without that delay, I noticed that the button wasn't always
registering the mouse click.

## Troubleshooting

click_play logs to "log.txt" in the same directory as the executable. It's fairly verbose, and
it includes timestamps. If you think that click_play didn't run when it was scheduled, look at
the logs.

## Limitations

### The Battle.net launcher window must be open.

click_play will restore the window if it's minimized, and it will bring it to the front so that
the button isn't obstructed; however, click_play won't open the window if it's closed.

### If your computer is locked, click_play won't run.

click_play runs as the current user. If there is no current user - e.g. because the computer is
locked - then click_play will not be run by the Task Scheduler. Because click_play manipulates the
mouse cursor, the only way around the afformentioned limitation is to store your windows
credentials with the task, and to allow the task to run with the highest privileges. I've opted to
forgo all of that. If you really want click_play to run when your computer is locked, you'll have
to create the task manually in Task Scheduler.

## Compiling

Using Visual Studio 2015, compiling should be as simple as building the solution.

### Dependencies

- json: https://github.com/nlohmann/json
- docopt.cpp: https://github.com/docopt/docopt.cpp
- spdlog: https://github.com/gabime/spdlog

All dependencies are in this repository.